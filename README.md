# **MS-TV-REST**

Micro Servicio de TV Production en NodeJs.

## **INSTALACIÓN**

* git clone https://gitlab.com/edgarsilvalovera/ms-tv-rest.git

* cd ms-tv-rest

* npm install

## **¿COMO LEVANTAR EL PROYECTO?**

* npm run start-dev

## Estructura del proyecto

```
├── src                    # Source files.
├── index.ts               # Página de índice.
├── package.json           
├── ormconfig.json          # Configuración del Orm para conexión a la Db
├── tsconfig.json          # Configuración compilador de ts.
├── .env.dist              # Variables de entorno (env.development, env.test, env.production)
└── README.md
```

### Source files

```
├── src                    # Source files.
|   ├── /config            # Contiene archivos de configuración (/database.ts, /environment.ts, /logger.ts, ...).
|   ├── /controllers       # Contiene archivos de clases para los controladores (nomenclatura: nameController.ts).
|   ├── /daos              # Contiene archivos de lógica de acceso a datos (nomenclatura: /nameDao.ts).
|   ├── /helpers
|   |   ├── /...
|   ├── /middlewares       # Contiene archivos de clases para los middlewares (nomenclatura: /name.ts).
|   ├── /models            # Contiene archivos de clases para los modelos (nomenclatura: /Name.ts).
└── └──  /services         # Contiene archivos de clases para los servicios (nomenclatura: nameService.ts).
└── └──  /WebServices      # Contiene archivos de clases para las conexiones con servicios externos (nomenclatura: nameWService.ts).
```
### Nomenclatura variables y métodos.

Se utiliza la convención [lowerCamelCase](https://developer.mozilla.org/en-US/docs/MDN/Contribute/Guidelines/Code_guidelines/JavaScript#Variable_naming):

