import * as express from 'express';
import * as bodyParser from "body-parser";
import * as jwt from "jsonwebtoken";
import controllers from './src/Controllers/index';
import ConectionOrm from "./src/Configs/ConectionOrm";
import envData from "./src/Configs/environment";
import logger from "./src/Configs/logger";

logger.debug(`Inicialización de ${process.env.APP_NAME}`);
logger.debug(`Enviroment: ${process.env.NODE_ENV}`);
logger.debug(`Inicializando - variables de entorno: ${JSON.stringify(envData)}`);

new ConectionOrm();

let api = express();
let port = process.env.PORT || 3002;

api.use(`/${process.env.BASE_END_POINT}`, bodyParser.json()); // support json encoded bodies

api.set('key', process.env.SECRET_KEY);

api.use(bodyParser.urlencoded({ extended: true }));

api.use(bodyParser.json());

api.listen(port, () => {    
    logger.debug(`Servidor inicializado en el puerto: ${port}`);
});

api.use(`${process.env.BASE_END_POINT}`, controllers);

api.post(`/accessToken`, (req, res) => {
    if(req.body.username === "Test" && req.body.password === "Test*01"){
        const payload = {
            check:  true
        };
        const token = jwt.sign(payload, api.get('key'), {
            expiresIn: 1440
        });

        res.json({
            token: token
        });
    }else{
        res.json({ mensaje: "Usuario y Clave Incorrectos"})
    }
})
  
export default api;