import envData from './src/Configs/environment';

const ormConfig = {
   type: "mariadb",

   host: envData.DATABASES_HOST, 

   port: Number(envData.DATABASES_PORT),

   username: envData.DATABASES_USER,

   password: envData.DATABASES_PASSWORD, 

   database: envData.DATABASES_NAME, 

   synchronize: false, 

   extra: { connectionLimit: envData.DATABASES_CONNECTION_LIMIT_POOL }, 

   logging: false, 

   entities: [
      "src/Models/DBEntity/**/*.{ts,js}"
   ],
   migrations: [
      "src/Migration/**/*.{ts,js}" 
   ],   
   cli: {
      "entitiesDir": "src/Models/DBEntity", 
      "migrationsDir": "src/Migration", 
   }
}
export default ormConfig;