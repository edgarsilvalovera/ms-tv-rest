import {createConnection} from "typeorm";
import ormConfig from '../../ormconfig';
import logger from "./logger";
export default class ConectionOrm{

    constructor(){
        logger.debug(`Variables de ormConfig: ${JSON.stringify(ormConfig)}`);
        this.connectionOrm(ormConfig);
    }

    async connectionOrm(ormConfig){
        return await createConnection(ormConfig).then(async connection => {            
            logger.debug(`Conexión de Base de Datos Creada`);
        })
        .catch(error => logger.error(`Error en Conexión de Base de Datos: ${error} `));
    }
}
