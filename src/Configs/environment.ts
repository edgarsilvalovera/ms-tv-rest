import * as Dotenv from 'dotenv';
import * as path from 'path';
import EnumEnvironments from "../Enums/EnumEnvironment";

let envData: Dotenv.DotenvParseOutput;
switch (process.env.NODE_ENV){
  case EnumEnvironments.DEV:    
    envData = Dotenv.config({path: `${path.join(process.env.PWD)}/.env.development`}).parsed;
    break;
  case EnumEnvironments.TEST:
    envData = Dotenv.config({path: `${path.join(process.env.PWD)}/.env.test`}).parsed;
    break;  
  case EnumEnvironments.PROD:
    envData = Dotenv.config({path: `${path.join(process.env.PWD)}/.env`}).parsed;
    break;
  default:         
    envData = Dotenv.config().parsed;
}
export default envData;