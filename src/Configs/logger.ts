import { Service } from 'typedi';

class LoggerType {
    public static REQUEST : string = "Request";
    public static RESPONSE : string = "Response";
    public static MESSAGE: string = "Message";
}

class LevelType {
    public static info : string = "info";
    public static error : string = "error";
    public static debug: string = "debug";
    public static warn: string = "warn";
}

interface Message {
    nameApp : string;
    originType : string;
    traceId : string | unknown;
    level : string;
    type : string;
    method : string;
    statusCode : number;
    domain : string;
    endpoint : string;
    url : string;
    content : string;
    tsLog: Date;
}


@Service()
class Logger {    
    constructor() {
        this.validateEnvironments();
    }

    debug(message: string) {
        let msg: Message = {
            nameApp : process.env.APP_NAME,
            originType : null,
            traceId : null,
            level : LevelType.debug,
            type : LoggerType.MESSAGE,
            method : null,
            statusCode : null,
            domain : null,
            endpoint : null,
            url : null,
            content : message,
            tsLog: new Date()
        };
        console.error(this.formatMessageConsole(msg))
    }

    info(message: string) {
        let msg: Message = {
            nameApp : process.env.APP_NAME,
            originType : null,
            traceId : null,
            level : LevelType.info,
            type : LoggerType.MESSAGE,
            method : null,
            statusCode : null,
            domain : null,
            endpoint : null,
            url : null,
            content : message,
            tsLog: new Date()
        };
        console.error(this.formatMessageConsole(msg))
    }

    error(message: string) {
        let msg: Message = {
            nameApp : process.env.APP_NAME,
            originType : null,
            traceId : null,
            level : LevelType.error,
            type : LoggerType.MESSAGE,
            method : null,
            statusCode : null,
            domain : null,
            endpoint : null,
            url : null,
            content : message,
            tsLog: new Date()
        };
        console.error(this.formatMessageConsole(msg))
    }

    request(origin: string, method: string, domain: string, endpoint: string, body: string, header: string) {
        let message: Message = {
            nameApp : process.env.APP_NAME,
            originType : origin,
            traceId : null,
            level : LevelType.info,
            type : LoggerType.REQUEST,
            method : method,
            statusCode : null,
            domain : domain,
            endpoint : endpoint,
            url : `${domain}${endpoint}`,
            content : `Headers: ${header} Body: ${body}`,
            tsLog: new Date()
        };
        console.info(this.formatMessageConsole(message))
    }

    response(origin: string, statusCode: number, body: string, header: string) {
        let message: Message = {
            nameApp : process.env.APP_NAME,
            originType : origin,
            traceId : null,
            level : LevelType.info,
            type : LoggerType.RESPONSE,
            method : null,
            statusCode : statusCode,
            domain : null,
            endpoint : null,
            url : null,
            content : `Headers: ${header} Body: ${body}`,
            tsLog: new Date()
        };
        console.info(this.formatMessageConsole(message))
    }    

    validateEnvironments() {
        if (!process.env.APP_NAME) {
            console.warn("Error Logger: The environment 'APP_NAME' is required");
        }
    }

    protected formatMessageConsole(message: Message) {
        let info = this.getInfo(message);
        let output = ``;
        output += message.traceId ? `${this.getAppName()} ${this.displayTimestamp(message.tsLog.toISOString())} ${this.displayTraceId(message.traceId)} | ${info}`
          : `${this.getAppName()} ${this.displayTimestamp(message.tsLog.toISOString())} | ${info}`;
        return output;
    }

    private getInfo = (message: Message): string => {
        switch (message.type) {
            case LoggerType.REQUEST:
                return `Type: ${LoggerType.REQUEST} | OriginType: ${message.originType} | Level: ${this.displayLevel(message.level)} | Method: ${message.method} | Domain: ${message.domain} | Endpoint: ${message.endpoint} | Content: ${message.content} `;
            case LoggerType.RESPONSE:
                return `Type: ${LoggerType.RESPONSE} | OriginType: ${message.originType} | Level: ${this.displayLevel(message.level)} | StatusCode: ${this.displayStatusCode(message.statusCode)} | Content: ${message.content}`;
            case LoggerType.MESSAGE:
            default:
                return `Type: ${LoggerType.MESSAGE} | Level: ${this.displayLevel(message.level)} | Content: ${message.content}`;
        }
    }

    private displayTimestamp = (timestamp: string): string => {
        return `[\u001b[36m${timestamp}\u001b[39m]`;
      }
  
      private displayTraceId = (traceId: string | unknown): string => {
          return traceId ? `[\u001b[36mtraceId:${traceId}\u001b[39m]` : "";
      }
  
      private getAppName = (): string => {
          return `[\u001b[35m${process.env.APP_NAME}\u001b[39m]`;
      }
  
      private displayStatusCode = (statusCode: number): string => {
          return `[\u001b[35m${statusCode.toString()}\u001b[39m]`;
      }
  
      private displayLevel = (level: string): string => {
          switch (level.toLowerCase()){
              //RED
              case "error":
                  return `[\u001b[31m${level}\u001b[39m]`;
              //GREEN
              case "info":
                  return `[\u001b[32m${level}\u001b[39m]`;
              //YELLOW
              case "warn":
                  return `[\u001b[33m${level}\u001b[39m]`;
              //BLUE    
              case "debug":
                  return `[\u001b[34m${level}\u001b[39m]`;
              //GREEN
              default:
                  return `[\u001b[32m${level}\u001b[39m]`;     
          }
              
      }
  
}
const logger = new Logger();
export default logger;