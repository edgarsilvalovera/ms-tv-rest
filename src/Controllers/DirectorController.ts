import { JsonController, HttpCode, Post, Body, Get, QueryParam } from "routing-controllers";
import EnumHttpCodes from "../Enums/EnumHttpCodes";
import logger from "../Configs/logger";
import OriginType from "../Models/Logger/OriginType";
import DirectorRequest from "../Models/Requests/Director/DirectorRequest";
import DirectorResponse from "../Models/Responses/Director/DirectorResponse";
import DirectorHandler from "../Handlers/DirectorHandler";
import { Director } from "../Models/DBEntity/Director";

@JsonController('/director')
export default class DirectorController{

    constructor(
        private _directorHandler: DirectorHandler
    ){}

    @HttpCode(EnumHttpCodes.CREATED)
    @Post()
    async createDirector(
        @Body() body: DirectorRequest
    ): Promise<DirectorResponse>{
        logger.info('Creando Directror');
        let response: DirectorResponse = await this._directorHandler.createDirector(body);
        logger.response(OriginType.internal, EnumHttpCodes.OK, JSON.stringify(response), '{}' );
        return response;
    }

    @HttpCode(EnumHttpCodes.OK)
    @Get()
    async getDirector(
        @QueryParam('name') name: string
    ): Promise<Director[]>{
        logger.info('Obteniendo Directror');
        let response: Director[] = await this._directorHandler.getDirector(name);
        logger.response(OriginType.internal, EnumHttpCodes.OK, JSON.stringify(response), '{}' );
        return response;
    }
}