import { JsonController, HttpCode, Post, Body, Get, QueryParams } from "routing-controllers";
import EnumHttpCodes from "../Enums/EnumHttpCodes";
import logger from "../Configs/logger";
import OriginType from "../Models/Logger/OriginType";
import TvProductionRequest from "../Models/Requests/TvProduction/TvProductionRequest";
import TvProductionResponse from "../Models/Responses/TvProduction/TvProductionResponse";
import TvProductionHandler from "../Handlers/TvProductionHandler";
import TvProductionParams from "../Models/Requests/TvProduction/TvProductionParams";

@JsonController('/tvProduction')
export default class TvProductionController{

    constructor(
        private _tvProductionHandler: TvProductionHandler
    ){}

    @HttpCode(EnumHttpCodes.CREATED)
    @Post()
    async createTvProduction(
        @Body() body: TvProductionRequest
    ): Promise<TvProductionResponse>{
        logger.info('Creando Tv Production');
        let response: TvProductionResponse = await this._tvProductionHandler.createTvProduction(body);
        logger.response(OriginType.internal, EnumHttpCodes.OK, JSON.stringify(response), '{}' );
        return response;
    }

    @HttpCode(EnumHttpCodes.OK)
    @Get()
    async getTvProduction(
        @QueryParams() params: TvProductionParams
    ): Promise<TvProductionResponse[]>{
        logger.info('Obteniendo Tv Production');        
        let response: TvProductionResponse[] = await this._tvProductionHandler.getTvProduction(params);
        logger.response(OriginType.internal, EnumHttpCodes.OK, JSON.stringify(response), '{}' );
        return response;
    }
}