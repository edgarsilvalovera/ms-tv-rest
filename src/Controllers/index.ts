import * as express from 'express';
import { useExpressServer, useContainer } from 'routing-controllers';
import { Container } from "typedi";
import { LoggerMiddleware } from '../Middleware/Logger';
import { UrlController } from '../Middleware/UrlNotFound';
import { ErrorController } from '../Middleware/Error';
import TvProductionController from './TvProductionController';
import DirectorController from './DirectorController';
import { AccessToken } from '../Middleware/AccessToken';

const httpContext = require('express-http-context');

useContainer(Container);

let router = express.Router();
router.use(httpContext.middleware);

useExpressServer(router, {    
    classTransformer: true,
    defaultErrorHandler: false,
    controllers: [DirectorController, TvProductionController],
    middlewares: [AccessToken,LoggerMiddleware, UrlController, ErrorController],
  });
  
  export default router;