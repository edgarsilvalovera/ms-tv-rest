import { Service } from 'typedi';
import {getRepository } from "typeorm";
import ErrorResponse from '../Models/Responses/ErrorResponse';
import EnumHttpCodes from '../Enums/EnumHttpCodes';
import { Actor } from '../Models/DBEntity/Actor';

@Service()
export default class ActorDao {
    
    constructor() {}

    async createActors(actors: Actor[]): Promise<Actor[] | ErrorResponse> {
        try {            
            return await getRepository(Actor).save(actors);
        } catch (error) {            
            return new ErrorResponse(EnumHttpCodes.INTERNAL_SERVER_ERROR, EnumHttpCodes.STATUS_INTERNAL_SERVER_ERROR, error.message);
        }
    }

    async getDirectorByDocument(document: string): Promise<Actor | ErrorResponse> {
        try {            
            return await getRepository(Actor).findOne({where: {document: document}})
        } catch (error) {
            return new ErrorResponse(EnumHttpCodes.INTERNAL_SERVER_ERROR, EnumHttpCodes.STATUS_INTERNAL_SERVER_ERROR, error.message);
        }
    }
}