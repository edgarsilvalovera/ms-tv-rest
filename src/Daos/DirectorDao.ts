import { Service } from 'typedi';
import {getRepository } from "typeorm";
import ErrorResponse from '../Models/Responses/ErrorResponse';
import EnumHttpCodes from '../Enums/EnumHttpCodes';
import { Director } from '../Models/DBEntity/Director';

@Service()
export default class DirectorDao {
    
    constructor() {}

    async createDirector(director: Director): Promise<Director | ErrorResponse> {
        try {            
            return await getRepository(Director).save(director);            
        } catch (error) {
            return new ErrorResponse(EnumHttpCodes.INTERNAL_SERVER_ERROR, EnumHttpCodes.STATUS_INTERNAL_SERVER_ERROR, error.message);
        }
    }

    async getDirectorByName(name: string): Promise<Director | ErrorResponse> {
        try {            
            return await getRepository(Director).findOne({where: {name: name}})
        } catch (error) {
            return new ErrorResponse(EnumHttpCodes.INTERNAL_SERVER_ERROR, EnumHttpCodes.STATUS_INTERNAL_SERVER_ERROR, error.message);
        }
    }

    async getDirectorById(id: number): Promise<Director | ErrorResponse> {
        try {            
            return await getRepository(Director).findOne({where: {id: id}})
        } catch (error) {
            return new ErrorResponse(EnumHttpCodes.INTERNAL_SERVER_ERROR, EnumHttpCodes.STATUS_INTERNAL_SERVER_ERROR, error.message);
        }
    }

    async getAllDirector(): Promise<Director[] | ErrorResponse> {
        try {            
            return await getRepository(Director).find({where: {enabled: 1}});
        } catch (error) {
            return new ErrorResponse(EnumHttpCodes.INTERNAL_SERVER_ERROR, EnumHttpCodes.STATUS_INTERNAL_SERVER_ERROR, error.message);
        }
    }
}