import { Service } from 'typedi';
import {getRepository } from "typeorm";
import ErrorResponse from '../Models/Responses/ErrorResponse';
import EnumHttpCodes from '../Enums/EnumHttpCodes';
import { Episode } from '../Models/DBEntity/Episode';

@Service()
export default class EpisodeDao {
    
    constructor() {}

    async createEpisodes(episodes: Episode[]): Promise<Episode[] | ErrorResponse> {
        try {
            return await getRepository(Episode).save(episodes);
        } catch (error) {            
            return new ErrorResponse(EnumHttpCodes.INTERNAL_SERVER_ERROR, EnumHttpCodes.STATUS_INTERNAL_SERVER_ERROR, error.message);
        }
    }
}