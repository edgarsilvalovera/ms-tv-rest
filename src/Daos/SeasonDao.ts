import { Service } from 'typedi';
import {getRepository } from "typeorm";
import ErrorResponse from '../Models/Responses/ErrorResponse';
import EnumHttpCodes from '../Enums/EnumHttpCodes';
import { Season } from '../Models/DBEntity/Season';

@Service()
export default class SeasonDao {
    
    constructor() {}

    async createSeasons(seasons: Season[]): Promise<Season[] | ErrorResponse> {
        try {            
            return await getRepository(Season).save(seasons);
        } catch (error) {            
            return new ErrorResponse(EnumHttpCodes.INTERNAL_SERVER_ERROR, EnumHttpCodes.STATUS_INTERNAL_SERVER_ERROR, error.message);
        }
    }
}