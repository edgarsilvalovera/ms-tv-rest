import { Service } from 'typedi';
import {getRepository } from "typeorm";
import ErrorResponse from '../Models/Responses/ErrorResponse';
import EnumHttpCodes from '../Enums/EnumHttpCodes';
import { TvProductionActor } from '../Models/DBEntity/TvProductionActor';

@Service()
export default class TvProductionActorDao {
    
    constructor() {}

    async createTvProductionActors(tvProductionActors: TvProductionActor[]): Promise<TvProductionActor[] | ErrorResponse> {
        try {            
            return await getRepository(TvProductionActor).save(tvProductionActors);
        } catch (error) {
            return new ErrorResponse(EnumHttpCodes.INTERNAL_SERVER_ERROR, EnumHttpCodes.STATUS_INTERNAL_SERVER_ERROR, error.message);
        }
    }
}