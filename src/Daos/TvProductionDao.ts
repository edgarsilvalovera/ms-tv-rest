import { Service } from 'typedi';
import {getRepository } from "typeorm";
import ErrorResponse from '../Models/Responses/ErrorResponse';
import EnumHttpCodes from '../Enums/EnumHttpCodes';
import { TvProduction } from '../Models/DBEntity/TvProduction';
import EnumTypeTvProduction from '../Enums/TvProduction/EnumTypeTvProduction';

@Service()
export default class TvProductionDao {
    
    constructor() {}

    async createTvProduction(tvProduction: TvProduction): Promise<TvProduction | ErrorResponse> {
        try {            
            return await getRepository(TvProduction).save(tvProduction);            
        } catch (error) {
            return new ErrorResponse(EnumHttpCodes.INTERNAL_SERVER_ERROR, EnumHttpCodes.STATUS_INTERNAL_SERVER_ERROR, error.message);
        }
    }

    async getTvProductionByName(name: string): Promise<TvProduction | ErrorResponse> {
        try {            
            return await getRepository(TvProduction).findOne({where: {name: name}})
        } catch (error) {
            return new ErrorResponse(EnumHttpCodes.INTERNAL_SERVER_ERROR, EnumHttpCodes.STATUS_INTERNAL_SERVER_ERROR, error.message);
        }
    }

    async getTvProductionByMovie(movie: string): Promise<TvProduction[] | ErrorResponse> {
        try {  
            return await getRepository(TvProduction).find({
                relations: ["typeTvProduction", "director", "tvProductionsActor", "seasons"],
                where: {
                    name: movie,
                    type_tv_production_id: EnumTypeTvProduction.MOVIE
                }
            });            
        } catch (error) {
            return new ErrorResponse(EnumHttpCodes.INTERNAL_SERVER_ERROR, EnumHttpCodes.STATUS_INTERNAL_SERVER_ERROR, error.message);
        }
    }
}