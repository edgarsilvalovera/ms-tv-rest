enum EnumDataTypeValidation {
    MIN_STRING = 1,
    MAX_STRING = 250,
    MIN_DATE = 19,
    MAX_DATE = 19,
    MAX_NUMBER = 2147483647,
    MAX_NUMBER_TYPE = 2,
    PASSWORD_REGEX = '/^[a-zA-Z0-9!@#$%^&*]$/',
    MAX_PASSWORD = 16,
    MIN_PASSWORD = 8,
    LOCALE_ES = 'es-ES',
    MIN_ACRONYM = 3,
    MAX_ACRONYM = 3,
    MIN_DOMAIN = 2,
    MAX_DOMAIN = 2
}

export default EnumDataTypeValidation;