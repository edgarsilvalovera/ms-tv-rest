enum EnumEnvironments{       
    DEV = "development",
    TEST = "test",    
    PROD = "production"
}
export default EnumEnvironments;