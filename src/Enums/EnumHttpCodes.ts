enum EnumHttpCodes {    
    OK = 200,
    STATUS_OK = 'OK',
    CREATED = 201,
    STATUS_CREATED = 'Created',
    BAD_REQUEST = 400,
    STATUS_BAD_REQUEST = 'Bad Request',
    UNAUTHORIZED = 401,
    STATUS_UNAUTHORIZED = 'Unauthorized',
    FORBIDDEN = 403,
    STATUS_FORBIDDEN = 'Forbidden',
    NOT_FOUND = 404,
    STATUS_NOT_FOUND = 'Not Found',
    INTERNAL_SERVER_ERROR = 500,
    STATUS_INTERNAL_SERVER_ERROR = 'Internal Server Error',
}

export default EnumHttpCodes;