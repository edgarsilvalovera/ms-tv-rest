enum EnumValidationsMessages {
    IS_NOT_EMPTY = 'El campo $property es obligatorio.',
    IS_POSITIVE = 'Debe ser mayor a 0.',
    IS_STRING = 'El campo $property debe ser de tipo string.',
    IS_BOOLEAN = 'El campo $property debe ser de tipo boolean.',
    IS_URL = 'El campo $property debe ser de tipo url. El formato de la url debe ser https://url.com',
    IS_NUMBER = 'El campo $property debe ser numérico.',
    IS_DATE = 'El campo $property debe ser de tipo fecha. El formato de la fecha debe ser: yyyy-mm-dd hh:mm:ss',
    NUMBER_MIN = 'El campo $property debe ser mayor a 0 (cero)',
    INT_MAX = 'El campo $property no debe ser mayor a 2147483647',
    STRING_MAX = 'El campo $property debe tener una longitud menor a $constraint1',
    IS_BETWEEN_DATES = 'El campo $property debe estar dentro del rango de +- $constraint1 días desde la fecha y hora actual',
    MAX_LENGTH = 'La longitud máxima de $property es $constraint1',
    IS_ALPHA = 'El campo $property solo puede contener letras.',
    MAX_MIN_LENGTH = 'La longitud de $property debe estar comprendida entre $constraint1 y $constraint2'
}
export default EnumValidationsMessages;