import ErrorResponse from "../Models/Responses/ErrorResponse";
import { Service } from "typedi";
import DirectorService from "../Services/DirectorService";
import DirectorRequest from "../Models/Requests/Director/DirectorRequest";
import DirectorResponse from "../Models/Responses/Director/DirectorResponse";
import { Director } from "../Models/DBEntity/Director";
import { BadRequestError, InternalServerError } from "routing-controllers";

@Service()
export default class DirectorHandler{
    public constructor(
        private _directorService: DirectorService,
    ){}

    public async createDirector(directorRequest: DirectorRequest): Promise<DirectorResponse>{
        let director: Director | ErrorResponse = await this._directorService.getDirectorByName(directorRequest.name)
        if(director instanceof ErrorResponse) throw new InternalServerError(director.message);
        if(director) throw new BadRequestError("El name del Director ya se Encuentra Registrado");

        let newDirector: Director | ErrorResponse = await this._directorService.createDirector(directorRequest);        
        if(newDirector instanceof ErrorResponse) throw new InternalServerError(newDirector.message);
            
        let {id , name} = newDirector;
        return new DirectorResponse(id, name);
    }
    
    public async getDirector(name: string): Promise<Director[]>{
        let director: Director[] | Director | ErrorResponse;
        
        if(name)
            director = await this._directorService.getDirectorByName(name);

        if(!director)        
            director = await this._directorService.getAllDirector();

        if(director instanceof ErrorResponse) throw new InternalServerError(director.message);
        
        return (director instanceof Director) ? [director] : director;
    }
}