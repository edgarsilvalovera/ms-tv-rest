import ErrorResponse from "../Models/Responses/ErrorResponse";
import { Service } from "typedi";
import DirectorService from "../Services/DirectorService";
import { Director } from "../Models/DBEntity/Director";
import { BadRequestError, InternalServerError } from "routing-controllers";
import TvProductionRequest from "../Models/Requests/TvProduction/TvProductionRequest";
import TvProductionResponse from "../Models/Responses/TvProduction/TvProductionResponse";
import { TvProduction } from "../Models/DBEntity/TvProduction";
import TvProductionService from "../Services/TvProductionService";
import EnumTypeTvProduction from "../Enums/TvProduction/EnumTypeTvProduction";
import { Actor } from "../Models/DBEntity/Actor";
import ActorService from "../Services/ActorService";
import { TvProductionActor } from "../Models/DBEntity/TvProductionActor";
import TvProductionActorService from "../Services/TvProductionActorService";
import SeasonRequest from "../Models/Requests/Season/SeasonRequest";
import { Episode } from "../Models/DBEntity/Episode";
import EpisodeService from "../Services/EpisodeService";
import SeasonService from "../Services/SeasonService";
import { Season } from "../Models/DBEntity/Season";
import TvProductionParams from "../Models/Requests/TvProduction/TvProductionParams";

@Service()
export default class TvProductionHandler{
    public constructor(
        private _tvProductionService: TvProductionService,
        private _directorServide: DirectorService,
        private _actorService: ActorService,
        private _tvProductionActorService: TvProductionActorService,
        private _seasonService: SeasonService,
        private _episodeService: EpisodeService  
    ){}

    public async createTvProduction(tvProductionRequest: TvProductionRequest): Promise<TvProductionResponse>{
        let {name, typeTvProductionId, directorId, actor, season} = tvProductionRequest;
        
        await this.getTvProductionByName(name);        
        await this.getDirectorById(directorId);
        await this.validateTypeProductionId(typeTvProductionId, season);
        
        let resultActors: Actor[] | ErrorResponse = await this._actorService.createActors(actor);
        if(resultActors instanceof ErrorResponse) throw new InternalServerError(resultActors.message);

        let resultTvProduction: TvProduction | ErrorResponse =  await this._tvProductionService.createTvProduction(name, typeTvProductionId, directorId);
        if(resultTvProduction instanceof ErrorResponse) throw new InternalServerError(resultTvProduction.message);

        let resultTvProductionActors: TvProductionActor[] | ErrorResponse = await this._tvProductionActorService.creatTvProductionActors(resultTvProduction, resultActors);
        if(resultTvProductionActors instanceof ErrorResponse) throw new InternalServerError(resultTvProductionActors.message);

        if(typeTvProductionId === EnumTypeTvProduction.TV_SHOW){
            let resultSeason: Season[] | ErrorResponse = await this._seasonService.createSeason(season,resultTvProduction);
            if(resultSeason instanceof ErrorResponse) throw new InternalServerError(resultSeason.message);

            let resultEpisodes: Episode[] | ErrorResponse =  await this._episodeService.createEpisodes(resultSeason, directorId);
        }
        return new TvProductionResponse(resultTvProduction.id);
    }

    public async getTvProduction(params: TvProductionParams):Promise<TvProductionResponse[]>{
        let {movie} = params;
        let result: TvProductionResponse[] | ErrorResponse;

        if(movie)
            result = await this._tvProductionService.getTvProductionByMovie(movie);        
        
        if(result instanceof ErrorResponse) throw new InternalServerError(result.message);
 
        return result;
    }

    async getTvProductionByName(name: string): Promise<TvProduction>{
        let tvProduction: TvProduction | ErrorResponse = await this._tvProductionService.getTvProductionByName(name);
        if(tvProduction instanceof ErrorResponse) throw new InternalServerError(tvProduction.message);
        if(tvProduction) throw new BadRequestError('El name Tv Production ya se Encuentra Registrado');
        return tvProduction;
    }

    async getDirectorById(id: number): Promise<Director>{
        let director: Director | ErrorResponse = await this._directorServide.getDirectorById(id);
        if(director instanceof ErrorResponse) throw new InternalServerError(director.message);
        if(!director) throw new BadRequestError('El directorId es Incorrecto');
        return director;
    }

    async validateTypeProductionId(typeTvProductionId: number, season: SeasonRequest[]): Promise<void>{
        if(! (typeTvProductionId === EnumTypeTvProduction.MOVIE || typeTvProductionId === EnumTypeTvProduction.TV_SHOW) )
            throw new BadRequestError("El typeTvProductionId debe ser: Película (1) ó Programa de Televisión (2)");

        if(typeTvProductionId == EnumTypeTvProduction.TV_SHOW && !season)
            throw new BadRequestError("El season es requerido para un Tv Production de tipo Programa de Televisión (2)");
    }
}