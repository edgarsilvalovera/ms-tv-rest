import * as express from 'express';
import * as jwt from "jsonwebtoken";
import {Middleware, ExpressMiddlewareInterface} from "routing-controllers";
import { Response, Request, NextFunction } from 'express';

let api = express();

@Middleware({ type: "before" })
export class AccessToken implements ExpressMiddlewareInterface {
    use(req: Request, res: Response, next: NextFunction){
        const token = req.headers['access-token'];

        if (token){
            jwt.verify(token, process.env.SECRET_KEY, (err, decoded) => {
                if(err){
                    return res.json({ mensaje: 'Token inválida' });    
                }else{                    
                    next();
                }
            });
        }else{
            res.send({ 
                mensaje: 'Token no Enviado' 
            });
        }
    }
}