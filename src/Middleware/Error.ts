import {Middleware, ExpressErrorMiddlewareInterface, BadRequestError, UnauthorizedError, ForbiddenError, NotFoundError, MethodNotAllowedError, InternalServerError} from "routing-controllers";
import { Response, Request, NextFunction } from 'express';
import logger from '../Configs/logger';
@Middleware({ type: 'after' })
export class ErrorController implements ExpressErrorMiddlewareInterface {
  error(error: any, request: Request, response: Response, next: NextFunction) {
    let {httpCode, message, name: status} = error;    
    logger.error(`Http code: ${httpCode} (${status}) - ${message}`);    
    
    let exceptionError = {
      "400": new BadRequestError(message),
      "401": new UnauthorizedError(message),
      "403": new ForbiddenError(message),
      "404": new NotFoundError(message),
      "405": new MethodNotAllowedError(message)
    }

    let err = exceptionError[httpCode] || new InternalServerError(message);       
    response.status(httpCode).json(err);
    response.end();
  }
}