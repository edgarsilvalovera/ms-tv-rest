import { Middleware, ExpressMiddlewareInterface } from 'routing-controllers';
import { Response, Request, NextFunction } from 'express';
import logger from '../Configs/logger';
import OriginType from '../Models/Logger/OriginType';
@Middleware({ type: 'before' })
export class LoggerMiddleware implements ExpressMiddlewareInterface {
  use(req: Request, res: Response, next: NextFunction) {    
    logger.request( OriginType.internal, req.method, req.get('host'), req.originalUrl, JSON.stringify(req.body), JSON.stringify(req.headers) );    
    next();
  }
}
