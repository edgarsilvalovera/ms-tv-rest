import {Middleware, ExpressMiddlewareInterface, NotFoundError} from "routing-controllers";
import { Response, Request, NextFunction } from 'express';
import logger from '../Configs/logger';
@Middleware({ type: "after" })
export class UrlController implements ExpressMiddlewareInterface {
    use(req: Request, res: Response, next: NextFunction){
        if(!res.headersSent) {            
            logger.error(`Recurso no encontrado`);
            let err = new NotFoundError("Recurso no valido");            
            res.status(404).json(err);
        }
        res.end(); 
    }
}