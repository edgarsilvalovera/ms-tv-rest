import {MigrationInterface, QueryRunner, Table, TableColumn, TableForeignKey} from "typeorm";

//columnas bases
let columnId: TableColumn = new TableColumn;
columnId.name = 'id';
columnId.type = 'int';
columnId.isPrimary = true;
columnId.isGenerated = true;
columnId.generationStrategy = 'increment';
columnId.isNullable = false;

let columnName: TableColumn  = new TableColumn;
columnName.name = 'name';
columnName.type = 'varchar';
columnName.length = '255';
columnName.isNullable = false;

let columnDocument: TableColumn  = new TableColumn;
columnDocument.name = 'document';
columnDocument.type = 'varchar';
columnDocument.length = '255';
columnDocument.isUnique = true;
columnDocument.isNullable = false;

let columnEnabled: TableColumn  = new TableColumn;
columnEnabled.name = "enabled";
columnEnabled.type = 'tinyint';
columnEnabled.length = '1';
columnEnabled.default = 1;
columnEnabled.isNullable = false;

let columnCreatedAt: TableColumn  = new TableColumn;
columnCreatedAt.name = 'created_at';
columnCreatedAt.type = 'TIMESTAMP';
columnCreatedAt.isNullable = false;
columnCreatedAt.default = 'now()';

let columnUpdatedAt: TableColumn  = new TableColumn;
columnUpdatedAt.name = 'updated_at';
columnUpdatedAt.type = 'TIMESTAMP';
columnCreatedAt.isNullable = true;

let columnDeletedAt: TableColumn  = new TableColumn;
columnDeletedAt.name = 'deleted_at';
columnDeletedAt.type = 'TIMESTAMP';
columnDeletedAt.isNullable = true;

let columnTypeTvProductionId: TableColumn = new TableColumn;
columnTypeTvProductionId.name = 'type_tv_production_id';
columnTypeTvProductionId.type = 'int';
columnTypeTvProductionId.isNullable = false;

let columnDirectorId: TableColumn = new TableColumn;
columnDirectorId.name = 'director_id';
columnDirectorId.type = 'int';
columnDirectorId.isNullable = false;

let columnActorId: TableColumn = new TableColumn;
columnActorId.name = 'actor_id';
columnActorId.type = 'int';
columnActorId.isNullable = false;

let columnTvProductionId: TableColumn = new TableColumn;
columnTvProductionId.name = 'tv_production_id';
columnTvProductionId.type = 'int';
columnTvProductionId.isNullable = false;

let columnSeasonId: TableColumn = new TableColumn;
columnSeasonId.name = 'season_id';
columnSeasonId.type = 'int';
columnSeasonId.isNullable = false;

let cascade: string = 'CASCADE';
let restrict: string = 'RESTRICT';

//create table type_tv_production
const typeTvProduction: Table = new Table;
typeTvProduction.name = 'type_tv_production';
typeTvProduction.columns = [columnId, columnName, columnCreatedAt, columnUpdatedAt, columnDeletedAt, columnEnabled];

//create table director
const director: Table = new Table;
director.name = 'director';
director.columns = [columnId, columnName, columnCreatedAt, columnUpdatedAt, columnDeletedAt, columnEnabled];

//create table tv_production
const tvProduction : Table = new Table;
tvProduction.name = 'tv_production';
tvProduction.columns = [columnId, columnName, columnTypeTvProductionId, columnDirectorId, columnCreatedAt, columnUpdatedAt, columnDeletedAt, columnEnabled];

//add FK tv_production
let fKTypeTvProductionId: TableForeignKey = new TableForeignKey({
    name: tvProduction.name,
    columnNames: [columnTypeTvProductionId.name],
    referencedTableName: typeTvProduction.name,
    referencedColumnNames: [columnId.name],
    onDelete: restrict,
    onUpdate: cascade

});
fKTypeTvProductionId.name = `fk_${columnTypeTvProductionId.name}`;
let fKDirectorId: TableForeignKey = new TableForeignKey({
    name: tvProduction.name,
    columnNames: [columnDirectorId.name],
    referencedTableName: director.name,
    referencedColumnNames: [columnId.name],
    onDelete: restrict,
    onUpdate: cascade

});
fKDirectorId.name = `fk_${columnDirectorId.name}`;
tvProduction.foreignKeys = [fKTypeTvProductionId, fKDirectorId];

//create table actor
const actor: Table = new Table();
actor.name = 'actor';
actor.columns = [columnId, columnName, columnDocument, columnCreatedAt, columnUpdatedAt, columnDeletedAt, columnEnabled];

//create table tv_production_actor
const tvProductionActor: Table = new Table();
tvProductionActor.name = 'tv_production_actor';
tvProductionActor.columns = [columnId, columnTvProductionId, columnActorId, columnCreatedAt, columnUpdatedAt, columnDeletedAt, columnEnabled];

//add FK tv_production_actor
let fKActorId: TableForeignKey = new TableForeignKey({
    name: tvProductionActor.name,
    columnNames: [columnActorId.name],
    referencedTableName: actor.name,
    referencedColumnNames: [columnId.name],
    onDelete: restrict,
    onUpdate: cascade

});
fKActorId.name = `fk_${columnActorId.name}`;
let fKTvProductionId: TableForeignKey = new TableForeignKey({
    name: tvProductionActor.name,
    columnNames: [columnTvProductionId.name],
    referencedTableName: tvProduction.name,
    referencedColumnNames: [columnId.name],
    onDelete: restrict,
    onUpdate: cascade

});
fKTvProductionId.name = `fk_${columnTvProductionId.name}`;
tvProductionActor.foreignKeys = [fKActorId,fKTvProductionId];

//create table season
const season: Table = new Table();
season.name = 'season';
season.columns = [columnId, columnName, columnTvProductionId, columnCreatedAt, columnUpdatedAt, columnDeletedAt, columnEnabled];

//add FK season
let fKTvProductionIdSeason: TableForeignKey = new TableForeignKey({
    name: season.name,
    columnNames: [columnTvProductionId.name],
    referencedTableName: tvProduction.name,
    referencedColumnNames: [columnId.name],
    onDelete: restrict,
    onUpdate: cascade

});
fKTvProductionIdSeason.name = `fk_${columnTvProductionId.name}_${season.name}`;
season.foreignKeys = [fKTvProductionIdSeason];

//create table episodes
const episode: Table = new Table();
episode.name = 'episode';
episode.columns = [columnId, columnName, columnSeasonId, columnDirectorId, columnCreatedAt, columnUpdatedAt, columnDeletedAt, columnEnabled];

//add FK episodes
let fKSeasonID: TableForeignKey = new TableForeignKey({
    name: episode.name,
    columnNames: [columnSeasonId.name],
    referencedTableName: season.name,
    referencedColumnNames: [columnId.name],
    onDelete: restrict,
    onUpdate: cascade

});
fKSeasonID.name = `fk_${columnSeasonId.name}`;

let fKDirectorIdEpisode: TableForeignKey = new TableForeignKey({
    name: episode.name,
    columnNames: [columnDirectorId.name],
    referencedTableName: director.name,
    referencedColumnNames: [columnId.name],
    onDelete: restrict,
    onUpdate: cascade

});
fKDirectorIdEpisode.name = `fk_${columnDirectorId.name}_${episode.name}`;
episode.foreignKeys = [fKSeasonID, fKDirectorIdEpisode];


export class DatabaseStructure1636544249510 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(typeTvProduction);
        await queryRunner.createTable(director);
        await queryRunner.createTable(tvProduction);
        await queryRunner.createTable(actor);
        await queryRunner.createTable(tvProductionActor);
        await queryRunner.createTable(season);
        await queryRunner.createTable(episode);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable(episode);
        await queryRunner.dropTable(season);
        await queryRunner.dropTable(tvProductionActor);
        await queryRunner.dropTable(actor);
        await queryRunner.dropTable(tvProduction);
        await queryRunner.dropTable(director);
        await queryRunner.dropTable(typeTvProduction);
    }

}
