import {MigrationInterface, QueryRunner} from "typeorm";

export class InsertTypeTvProduction1636585913406 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {

        await queryRunner.query("INSERT INTO `type_tv_production` (id, name) values (1, 'Película'), (2, 'Programa de Telivisión');");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("DELETE FROM `type_tv_production` WHERE id IN (1,2);");
    }

}
