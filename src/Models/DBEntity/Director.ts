import {Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToMany} from "typeorm";
import { Episode } from "./Episode";
import { TvProduction } from "./TvProduction";

@Entity()
export class Director {  
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;
    
    @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
    created_at: Date;

    @UpdateDateColumn({ type: "timestamp", onUpdate: "CURRENT_TIMESTAMP" })
    updated_at: Date;

    @Column("timestamp")
    deleted_at: Date;

    @Column({ nullable: false, default: true })
    enabled: boolean;
    
    @OneToMany(() => TvProduction, tvProductions => tvProductions.director)
    tvProductions: TvProduction[];

    @OneToMany(() => Episode, episodes => episodes.director)
    episodes: Episode[];
}