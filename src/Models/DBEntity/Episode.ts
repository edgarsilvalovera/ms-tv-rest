import {Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn} from "typeorm";
import { Director } from "./Director";
import { Season } from "./Season";

@Entity()
export class Episode {  
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    season_id: number;

    @Column()
    director_id: number;
    
    @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
    created_at: Date;

    @UpdateDateColumn({ type: "timestamp", onUpdate: "CURRENT_TIMESTAMP" })
    updated_at: Date;

    @Column("timestamp")
    deleted_at: Date;

    @Column({ nullable: false, default: true })
    enabled: boolean;

    @ManyToOne(() => Season, season => season.episodes)
    @JoinColumn({ name: "season_id" })
    season: Season;

    @ManyToOne(() => Director, director => director.tvProductions)
    @JoinColumn({ name: "director_id" })
    director: Director;
}
