import {Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn, OneToMany} from "typeorm";
import { Episode } from "./Episode";
import { TvProduction } from "./TvProduction";

@Entity()
export class Season {  
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    tv_production_id: number;
    
    @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
    created_at: Date;

    @UpdateDateColumn({ type: "timestamp", onUpdate: "CURRENT_TIMESTAMP" })
    updated_at: Date;

    @Column("timestamp")
    deleted_at: Date;

    @Column({ nullable: false, default: true })
    enabled: boolean;

    @ManyToOne(() => TvProduction, tvProduction => tvProduction.seasons)
    @JoinColumn({ name: "tv_production_id" })
    tvProduction: TvProduction;
    
    @OneToMany(() => Episode, episodes => episodes.season)
    episodes: Episode[];
}
