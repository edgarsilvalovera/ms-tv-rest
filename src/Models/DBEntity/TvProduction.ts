import {Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn, OneToMany} from "typeorm";
import { Director } from "./Director";
import { Season } from "./Season";
import { TvProductionActor } from "./TvProductionActor";
import { TypeTvProduction } from "./TypeTvProduction";

@Entity()
export class TvProduction {  
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    type_tv_production_id: number;

    @Column()
    director_id: number;
    
    @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
    created_at: Date;

    @UpdateDateColumn({ type: "timestamp", onUpdate: "CURRENT_TIMESTAMP" })
    updated_at: Date;

    @Column("timestamp")
    deleted_at: Date;

    @Column({ nullable: false, default: true })
    enabled: boolean;

    @ManyToOne(() => TypeTvProduction, typeTvProduction => typeTvProduction.typeTvProductions)
    @JoinColumn({ name: "type_tv_production_id" })
    typeTvProduction: TypeTvProduction;

    @ManyToOne(() => Director, director => director.tvProductions)
    @JoinColumn({ name: "director_id" })
    director: Director;

    @OneToMany(() => TvProductionActor, tvProductionsActor => tvProductionsActor.tvProduction)
    tvProductionsActor: TvProductionActor[];

    @OneToMany(() => Season, seasons => seasons.tvProduction)
    seasons: Season[];
}
