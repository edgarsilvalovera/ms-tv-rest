import {Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn} from "typeorm";
import { Actor } from "./Actor";
import { TvProduction } from "./TvProduction";

@Entity()
export class TvProductionActor {  
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    tv_production_id: number;

    @Column()
    actor_id: number;
    
    @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
    created_at: Date;

    @UpdateDateColumn({ type: "timestamp", onUpdate: "CURRENT_TIMESTAMP" })
    updated_at: Date;

    @Column("timestamp")
    deleted_at: Date;

    @Column({ nullable: false, default: true })
    enabled: boolean;

    @ManyToOne(() => TvProduction, tvProduction => tvProduction.tvProductionsActor)
    @JoinColumn({ name: "tv_production_id" })
    tvProduction: TvProduction;

    @ManyToOne(() => Actor, actor => actor.tvProductionsActor)
    @JoinColumn({ name: "actor_id" })
    actor: Actor;
}
