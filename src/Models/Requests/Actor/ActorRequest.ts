import { IsString, Length, IsDefined } from "class-validator";
import EnumValidationsMessages from "../../../Enums/EnumValidationsMessages";
import { Expose } from "class-transformer";
import EnumDataTypeValidation from "../../../Enums/EnumDataTypeValidation";

export default class ActorRequest{

    @IsDefined({ message: EnumValidationsMessages.IS_NOT_EMPTY })
    @IsString({ message: EnumValidationsMessages.IS_STRING })
    @Length( EnumDataTypeValidation.MIN_STRING, EnumDataTypeValidation.MAX_STRING, { message: EnumValidationsMessages.MAX_LENGTH})
    @Expose({ name: 'name' })
    name: string;

    @IsDefined({ message: EnumValidationsMessages.IS_NOT_EMPTY })
    @IsString({ message: EnumValidationsMessages.IS_STRING })
    @Length( EnumDataTypeValidation.MIN_STRING, EnumDataTypeValidation.MAX_STRING, { message: EnumValidationsMessages.MAX_LENGTH})
    @Expose({ name: 'document' })
    document: string;
}