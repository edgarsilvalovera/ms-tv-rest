import { IsString, Length} from "class-validator";
import EnumValidationsMessages from "../../../Enums/EnumValidationsMessages";
import { Expose } from "class-transformer";
import EnumDataTypeValidation from "../../../Enums/EnumDataTypeValidation";

export default class TvProductionParams{    
    @IsString({ message: EnumValidationsMessages.IS_STRING })
    @Length( EnumDataTypeValidation.MIN_STRING, EnumDataTypeValidation.MAX_STRING, { message: EnumValidationsMessages.MAX_LENGTH})
    @Expose({ name: 'movie' })
    movie: string;
    
    @IsString({ message: EnumValidationsMessages.IS_STRING })
    @Length( EnumDataTypeValidation.MIN_STRING, EnumDataTypeValidation.MAX_STRING, { message: EnumValidationsMessages.MAX_LENGTH})
    @Expose({ name: 'tvShow' })
    tvShow: string

    @IsString({ message: EnumValidationsMessages.IS_STRING })
    @Length( EnumDataTypeValidation.MIN_STRING, EnumDataTypeValidation.MAX_STRING, { message: EnumValidationsMessages.MAX_LENGTH})
    @Expose({ name: 'episode' })
    episode: string
}