import { IsString, Length, IsDefined, IsNumber, IsPositive, Max } from "class-validator";
import EnumValidationsMessages from "../../../Enums/EnumValidationsMessages";
import { Expose } from "class-transformer";
import EnumDataTypeValidation from "../../../Enums/EnumDataTypeValidation";
import ActorRequest from "../Actor/ActorRequest";
import SeasonRequest from "../Season/SeasonRequest";

export default class TvProductionRequest{

    @IsDefined({ message: EnumValidationsMessages.IS_NOT_EMPTY })
    @IsString({ message: EnumValidationsMessages.IS_STRING })
    @Length( EnumDataTypeValidation.MIN_STRING, EnumDataTypeValidation.MAX_STRING, { message: EnumValidationsMessages.MAX_LENGTH})
    @Expose({ name: 'name' })
    name: string;

    @IsDefined({ message: EnumValidationsMessages.IS_NOT_EMPTY })
    @IsNumber({ allowNaN: false }, { message: EnumValidationsMessages.IS_NUMBER })
    @IsPositive({ message: EnumValidationsMessages.IS_POSITIVE })
    @Max(EnumDataTypeValidation.MAX_NUMBER_TYPE, {message: EnumValidationsMessages.INT_MAX})
    @Expose({ name: 'typeTvProductionId' })
    typeTvProductionId: number

    @IsDefined({ message: EnumValidationsMessages.IS_NOT_EMPTY })
    @IsNumber({ allowNaN: false }, { message: EnumValidationsMessages.IS_NUMBER })
    @IsPositive({ message: EnumValidationsMessages.IS_POSITIVE })    
    @Expose({ name: 'directorId' })
    directorId: number

    @IsDefined({ message: EnumValidationsMessages.IS_NOT_EMPTY })
    @Expose({ name: 'actor' })
    actor: ActorRequest[]
    
    @Expose({ name: 'season' })
    season: SeasonRequest[]
}