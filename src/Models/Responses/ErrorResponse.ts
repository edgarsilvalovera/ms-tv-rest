export default class ErrorResponse {
    code: number;
    status: string;
    message: string;

    constructor(code: number, status: string, message: string) {    
        this.code = code;
        this.status = status;
        this.message = message;
    }
}