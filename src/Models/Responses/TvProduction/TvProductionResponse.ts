import SeasonResponse from "./SeasonResponse";

export default class TvProductionResponse{
    id: number;
    name: string;
    typeTvProduction: string;
    nameDirector: string;    
    season: SeasonResponse[];

    constructor(id: number){
        this.id = id;        
    }
}