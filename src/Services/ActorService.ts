import { Service } from 'typedi';
import ActorDao from '../Daos/ActorDao';
import { Actor } from '../Models/DBEntity/Actor';
import ActorRequest from '../Models/Requests/Actor/ActorRequest';
import ErrorResponse from '../Models/Responses/ErrorResponse';

@Service()
export default class ActorService {
  constructor(private _actorDao: ActorDao) {}

  async getDirectorByDocument(document: string): Promise<Actor | ErrorResponse> {
    return await this._actorDao.getDirectorByDocument(document);
  }
  
  async createActors(actorsRequest: ActorRequest[]): Promise<Actor[] | ErrorResponse>{
    let actors: Actor[] = [];

    await Promise.all(
      actorsRequest.map( async element => {
        let {document} = element;
        let newActor: Actor = new Actor;
  
        let  resultActor: Actor | ErrorResponse = await this._actorDao.getDirectorByDocument(document);
        if(resultActor instanceof ErrorResponse) return resultActor;
  
        if(resultActor)
          newActor = {...newActor, ...resultActor};        
        else
          newActor = {...newActor, ...element};
        
        actors = [...actors, newActor];        
      })
    );
    return await this._actorDao.createActors(actors);
  }
}