import { Service } from 'typedi';
import DirectorDao from '../Daos/DirectorDao';
import { Director } from '../Models/DBEntity/Director';
import DirectorRequest from '../Models/Requests/Director/DirectorRequest';
import ErrorResponse from '../Models/Responses/ErrorResponse';

@Service()
export default class DirectorService {
  constructor(private _directorDao: DirectorDao) {}

  buildDirector(director: DirectorRequest): Director{
    let newDirector = new Director();
    newDirector.name = director.name;    
    return newDirector;
  }

  async createDirector(director: DirectorRequest): Promise<Director | ErrorResponse> {    
    let newDirector = this.buildDirector(director);
    return await this._directorDao.createDirector(newDirector);
  }

  async getDirectorByName(name: string): Promise<Director | ErrorResponse> {
    return await this._directorDao.getDirectorByName(name);
  }

  async getDirectorById(id: number): Promise<Director | ErrorResponse> {
    return await this._directorDao.getDirectorById(id);
  }

  async getAllDirector(): Promise<Director[] | ErrorResponse> {
    return await this._directorDao.getAllDirector();
  }
}