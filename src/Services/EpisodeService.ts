import { Service } from 'typedi';
import EpisodeDao from '../Daos/EpisodeDao';
import { Episode } from '../Models/DBEntity/Episode';
import { Season } from '../Models/DBEntity/Season';
import ErrorResponse from '../Models/Responses/ErrorResponse';

@Service()
export default class EpisodeService {
  constructor(private _episodeDao: EpisodeDao) {}
  
  async createEpisodes(seasons: Season[], directorId: number): Promise<Episode[] | ErrorResponse>{
    let newEpisodes: Episode[] = [];

    await Promise.all(
      seasons.map( async element => {
        let {episodes, id: seasonId} = element;
        episodes.map(e => {
          e.season_id = seasonId;
          e.director_id = directorId;
        });
        newEpisodes = [...newEpisodes, ...episodes];    
      })
    );
    return await this._episodeDao.createEpisodes(newEpisodes);
  }
}