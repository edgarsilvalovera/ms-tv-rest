import { Service } from 'typedi';
import SeasonDao from '../Daos/SeasonDao';
import { Episode } from '../Models/DBEntity/Episode';
import { Season } from '../Models/DBEntity/Season';
import { TvProduction } from '../Models/DBEntity/TvProduction';
import SeasonRequest from '../Models/Requests/Season/SeasonRequest';
import ErrorResponse from '../Models/Responses/ErrorResponse';

@Service()
export default class SeasonService {
  constructor(
    private _seasonDao: SeasonDao
  ) {}
  
  async createSeason(seasonRequest: SeasonRequest[], tvProduction:TvProduction): Promise<Season[] | ErrorResponse>{    
    let seasons: Season[] = [];

    await Promise.all(
      seasonRequest.map( async element => {
        let {episode, name} = element;
        let episodes: Episode[] = [];

        await Promise.all(
          episode.map( async e => {
            let {name} = e;
            let newEpisode: Episode = new Episode;
            newEpisode.name = name;            
            episodes = [...episodes, newEpisode];
          })
        );

        let newSeason: Season = new Season;
        newSeason.name = name;
        newSeason.tvProduction = tvProduction;
        newSeason.episodes = episodes;

        seasons = [...seasons, newSeason];
      })
    );
    return await this._seasonDao.createSeasons(seasons);
  }
}