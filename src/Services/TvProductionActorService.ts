import { Service } from 'typedi';
import TvProductionActorDao from '../Daos/TvProductionActorDao';
import { Actor } from '../Models/DBEntity/Actor';
import { TvProduction } from '../Models/DBEntity/TvProduction';
import { TvProductionActor } from '../Models/DBEntity/TvProductionActor';
import ErrorResponse from '../Models/Responses/ErrorResponse';

@Service()
export default class TvProductionActorService {
  constructor(    
    private _tvProductionActorDao: TvProductionActorDao
  ) { }

  async creatTvProductionActors(tvProduction: TvProduction, actors: Actor[]): Promise<TvProductionActor[] | ErrorResponse>{    
    let tvProductionActors: TvProductionActor[] = [];
    await actors.forEach( elemnt => {
      let newTvProductionActor: TvProductionActor = new TvProductionActor;
      newTvProductionActor.actor = elemnt;
      newTvProductionActor.tvProduction = tvProduction;
      tvProductionActors = [...tvProductionActors, newTvProductionActor];      
    });
    return await this._tvProductionActorDao.createTvProductionActors(tvProductionActors);
  }
}