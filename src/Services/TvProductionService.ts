import { Service } from 'typedi';
import TvProductionDao from '../Daos/TvProductionDao';
import { TvProduction } from '../Models/DBEntity/TvProduction';
import ErrorResponse from '../Models/Responses/ErrorResponse';
import SeasonResponse from '../Models/Responses/TvProduction/SeasonResponse';
import TvProductionResponse from '../Models/Responses/TvProduction/TvProductionResponse';

@Service()
export default class TvProductionService {
  constructor(
    private _tvProductionDao: TvProductionDao    
  ) { }

  async createTvProduction(name: string, typeTvProductionId: number, directorId: number): Promise<TvProduction | ErrorResponse>{
    let newTvProduction = new TvProduction();
    newTvProduction.name = name;
    newTvProduction.type_tv_production_id = typeTvProductionId;
    newTvProduction.director_id = directorId;
    return await this._tvProductionDao.createTvProduction(newTvProduction);
  }

  async getTvProductionByName(name: string): Promise<TvProduction | ErrorResponse> {
    return await this._tvProductionDao.getTvProductionByName(name);
  }

  buildTvProductionResponse(tvProduction: TvProduction[]):TvProductionResponse[]{
    let response: TvProductionResponse[] = [];
    tvProduction.forEach( t => {
        let {id, name, seasons, director:{name: nameDirector}, typeTvProduction:{name: nameTypeTvProduction}} = t;
        
        let nameSeasons: SeasonResponse[] = [];
        seasons.forEach( s => {
            let seasonResponse: SeasonResponse = new SeasonResponse;
            seasonResponse.name = s.name;
            nameSeasons = [...nameSeasons, seasonResponse];
        });

        let tvProductionResponse: TvProductionResponse = new TvProductionResponse(id);
        tvProductionResponse.name = name;
        tvProductionResponse.nameDirector = nameDirector;
        tvProductionResponse.season = nameSeasons;
        tvProductionResponse.typeTvProduction = nameTypeTvProduction;

        response = [...response, tvProductionResponse];
    });
    return response;
  }

  async getTvProductionByMovie(movie: string): Promise<TvProductionResponse[] | ErrorResponse> {
    let tvProduction: TvProduction[] | ErrorResponse =  await this._tvProductionDao.getTvProductionByMovie(movie);
    if(tvProduction instanceof ErrorResponse) return tvProduction;
    return this.buildTvProductionResponse(tvProduction);
  }
}